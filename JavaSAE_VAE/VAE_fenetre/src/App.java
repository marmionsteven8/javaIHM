import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.Hyperlink;

public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Enchere");

        GridPane fenetre = new GridPane();

        // Gestion du bandeau haut ( header ) de la page
        HBox header = new HBox();
        Label logo = new Label("Insérer logo VAE");
        TextField recherche = new TextField();
        Button inscription = new Button("S'inscrire");
        Button identification = new Button("S'identitifer");
        Button aide = new Button("Besoin d'aide ?");
        inscription.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        identification.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        aide.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        header.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
        header.getChildren().addAll(logo, recherche, inscription, identification, aide);

        HBox menuDeroulant = new HBox(10);
        Button categorie1 = new Button("Categorie1");
        Button categorie2 = new Button("Categorie2");
        Button categorie3 = new Button("Categorie3");
        Button categorie4 = new Button("Categorie4");
        Button categorie5 = new Button("Categorie5");
        categorie1.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        categorie2.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        categorie3.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        categorie4.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        categorie5.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        menuDeroulant.setAlignment(Pos.CENTER);
        menuDeroulant.getChildren().addAll(categorie1, categorie2, categorie3, categorie4, categorie5);

        GridPane.setConstraints(header, 0, 0);
        GridPane.setConstraints(menuDeroulant, 0, 1);
        fenetre.getChildren().addAll(header, menuDeroulant);
    
        Scene scene = new Scene(fenetre);
        stage.setScene(scene);
        stage.show();
    }
}