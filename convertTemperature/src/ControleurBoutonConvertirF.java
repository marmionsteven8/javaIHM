import javafx.event.EventHandler;
import javafx.event.ActionEvent;


public class ControleurBoutonConvertirF implements EventHandler<ActionEvent>{ 

    private AppliConverter appli;
    
    public ControleurBoutonConvertirF(AppliConverter appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.convertitF();
    }
}
