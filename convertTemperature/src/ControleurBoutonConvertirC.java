import javafx.event.EventHandler;
import javafx.event.ActionEvent;


public class ControleurBoutonConvertirC implements EventHandler<ActionEvent>{ 

    private AppliConverter appli;
    
    public ControleurBoutonConvertirC(AppliConverter appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.convertitC();
    }
}
