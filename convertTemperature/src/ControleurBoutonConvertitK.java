import javafx.event.EventHandler;
import javafx.event.ActionEvent;


public class ControleurBoutonConvertitK implements EventHandler<ActionEvent>{ 

    private AppliConverter appli;
    
    public ControleurBoutonConvertitK(AppliConverter appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        this.appli.convertitK();
    }
}
