// javac --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls -d bin src/*.java
// java -cp bin:img --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls AppliConverter



import java.sql.Connection;

import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;

public class FenetreConnexion extends BorderPane {

    private BorderPane rootHautDePage;
    private VBox rootSousDePage;
    private TilePane images;

    private Scene scene;

    public FenetreConnexion(){
        this.rootHautDePage = new BorderPane();

        this.rootSousDePage = new VBox();

        this.images = new TilePane();
        // Construction du graphe de scène
        BorderPane pane = new BorderPane();

        Button deconnexion = new Button("Déconnexion", new ImageView("file:graphics/user.png"));
        deconnexion.setOnAction(new ControleurDeconnexion(this));
        Text text = new Text("Allo 45 - Module Analyste");

        Text textSousDePage = new Text("Analyse du sondage sur les habitudes alimentaires");
        ComboBox<String> pie = new ComboBox<>();
        PieChart chart = new PieChart();
        chart.setTitle("Que lisez-vous au petit déjeuner ?");
        chart.getData().setAll(
            new PieChart.Data(" Le journal " , 21),
            new PieChart.Data(" Un livre " , 3),
            new PieChart.Data(" Le courier " , 7),
            new PieChart.Data(" La boîte de céréales" , 75));
        chart.setLegendSide (Side.LEFT); // pour mettre la l é gende à gauche
        HBox deuxBoutons = new HBox();
        Button precedent = new Button("Précedent", new ImageView("file:graphics/back.png"));
        Button suivant = new Button("Suivant", new ImageView("file:graphics/next.png"));
        deuxBoutons.getChildren().addAll(precedent, suivant);


        this.rootHautDePage.setLeft(text);
        this.rootHautDePage.setRight(deconnexion);
        this.rootHautDePage.setBackground(new Background(new BackgroundFill (Color.LIGHTBLUE, null, null)));
        this.rootSousDePage.getChildren().addAll(textSousDePage, pie, chart, deuxBoutons);
        for (int i=1; i<9; ++i) {
            this.images.getChildren().add(new ImageView("file:graphics/chart_"+i+".png"));
        }

        text.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        //textSousDePage.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        pane.setTop(this.rootHautDePage);
        pane.setCenter(this.rootSousDePage);
        pane.setRight(this.images);


        this.scene = new Scene(pane);
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    public void connexionFenetreConnexion() {
        //AppliConnexion root = new AppliConnexion();
        //this.scene.setRoot(root);
    }
}