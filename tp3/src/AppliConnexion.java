import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Platform;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;



public class AppliConnexion extends Application {
    private Label messageConsignes;
    private TextField id;
    private Label idC;
    private TextField password;
    private Label pw;

    private Scene scene;
    
    @Override
    public void init(){
        // cette méthode est utilisée pour initialiser les éléments 
        // non graphiques et événetuellement graphiques autres que la Scène et le Stage
        this.messageConsignes = new Label("Entrez votre identifiant et votre mot de passe");
        this.id = new TextField();
        this.idC = new Label("Identifiant");
        this.password = new TextField();
        this.pw = new Label("Mot de passe");     
    }

    private GridPane gridPane(){
        GridPane pane = new GridPane();     
        pane.add(this.messageConsignes, 0, 0);
        pane.add(this.id, 1, 1);
        pane.add(this.idC, 0, 1);
        pane.add(this.password, 1, 2);  
        pane.add(this.pw, 0, 2); 
        pane.setHgap(0);
        pane.setVgap(15);    
        return pane;
    }

    private HBox hbox(){
        HBox pane =new HBox();
        Button connexion =new Button("Connexion");
        pane.getChildren().add(connexion); 
        connexion.setOnAction(new ControleurConnexion(this));
        return pane;
    }

    private HBox root(){
        HBox pane = new HBox();
        VBox vbox = new VBox();
        vbox.getChildren().addAll(this.gridPane(), this.hbox());
        vbox.setPrefWidth(500);
        HBox.setMargin(vbox, new Insets(50));
        pane.getChildren().add(vbox);
        return pane;
    }
    
    @Override
    public void start(Stage stage){              
        this.scene =new Scene(root());
        stage.setTitle("Connexion");
        stage.setScene(scene);
        stage.show();
    }
  
    /**
     * Cette méthode efface les deux textfields et le label résultat
     * en y mettant une chaine de caractère vide
     */
    public void efface(){
        this.id.clear();
        this.password.clear();
    }
    public String getId() throws NumberFormatException{
        return this.id.getText();
    }

    public String getPassword() throws NumberFormatException{
        return this.password.getText();
    } 
    public void connexionFenetreGraphique() {
        FenetreConnexion root = new FenetreConnexion();
        this.scene.setRoot(root);
    }
}
