import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurConnexion implements EventHandler<ActionEvent>{
    private AppliConnexion app;
    public ControleurConnexion(AppliConnexion app) {
        this.app=app;
    }
    @Override
    public void handle(ActionEvent event) {
        this.app.connexionFenetreGraphique();
    }
}
