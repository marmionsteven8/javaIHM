import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurDeconnexion implements EventHandler<ActionEvent>{
    private FenetreConnexion app;
    public ControleurDeconnexion(FenetreConnexion app) {
        this.app=app;
    }
    @Override
    public void handle(ActionEvent event) {
        this.app.connexionFenetreConnexion();
    }
}
