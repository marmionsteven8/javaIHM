// javac --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls -d bin src/*.java
// java -cp bin:img --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls AppliConverter

import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AppliCalculatrice extends Application {
    private TextField textFieldVal1;
    private TextField textFieldVal2;
    private Label res;

    @Override
    public void init(){
        // Initialisation des objects qui ne sont pas dans la scène
        // cad des éléments non graphiques
        this.textFieldVal1 = new TextField();
        this.textFieldVal2 = new TextField();
        this.res = new Label("Résultat : ");
    }

    @Override
    public void start(Stage stage) throws Exception{
        // Construction du graphe de scène
        VBox root = new VBox();
        HBox rootText = new HBox();
        HBox rootRes = new HBox();

        //TextField textFieldVal1 = new TextField();
        //TextField textFieldVal2 = new TextField();
        rootText.setAlignment(Pos.CENTER);
        rootRes.setAlignment(Pos.CENTER);

        rootText.getChildren().add(this.textFieldVal1);
        rootText.getChildren().add(this.textFieldVal2);
        rootRes.getChildren().add(this.res);
        root.getChildren().addAll(rootText);
        this.ajouteBoutons(root);
        root.getChildren().addAll(rootRes);


        Scene scene = new Scene(root, 300, 300);
        stage.setTitle("Calculette");
        stage.setScene(scene);
        stage.show();
    }
    private void ajouteBoutons(Pane root){
        HBox rootBtn = new HBox(); 
        Button btnAddition = new Button("+");
        Button btnSoustraire = new Button("-");

        btnAddition.setOnAction(new ControleurPlus(this));
        btnSoustraire.setOnAction(new ControleurMoins(this));

        rootBtn.setAlignment(Pos.CENTER);

        rootBtn.getChildren().addAll(btnAddition, btnSoustraire);
        root.getChildren().add(rootBtn);
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
    public double getValueLeft() throws NumberFormatException{
        return Double.parseDouble(this.textFieldVal1.getText());
    }

    public double getValueRight() throws NumberFormatException{
        return Double.parseDouble(this.textFieldVal2.getText());
    } 
    public void majTF(){
        this.textFieldVal1.setText(String.valueOf(this.getValueLeft()));
        this.textFieldVal2.setText(String.valueOf(this.getValueRight()));
    }
    public double addition() {
        double resLeft = this.getValueLeft();
        double resRight = this.getValueRight();
        this.res.setText(String.valueOf(resLeft + resRight));
        return resLeft + resRight;
    }
    public double soustraction() {
        double resLeft = this.getValueLeft();
        double resRight = this.getValueRight();
        this.res.setText(String.valueOf(resLeft - resRight));
        return resLeft - resRight;
    }
}