import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurPlus implements EventHandler<ActionEvent>{
    private AppliCalculatrice app;
    public ControleurPlus(AppliCalculatrice app) {
        this.app=app;
    }
    @Override
    public void handle(ActionEvent event) {
        this.app.addition();
    }
}
