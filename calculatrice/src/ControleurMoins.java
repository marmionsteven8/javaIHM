import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurMoins implements EventHandler<ActionEvent>{
    private AppliCalculatrice app;
    public ControleurMoins(AppliCalculatrice app) {
        this.app=app;
    }
    @Override
    public void handle(ActionEvent event) {
        this.app.soustraction();
    }
}
