import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurLancerPartie implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     **/
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param p vue du jeu
     */
    public ControleurLancerPartie(MotMystere modelePendu, Pendu vuePendu) {
        this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button radiobouton = (Button) actionEvent.getTarget();
        String nomDuRadiobouton = radiobouton.getText();
        if (nomDuRadiobouton.equals("Lancer une partie")) {
            System.out.println("Lancement de la partie");
            int niveau = this.modelePendu.getNiveau();
            this.modelePendu.setNiveau(niveau);
            this.modelePendu.setMotATrouver();
            this.vuePendu.majAffichage();
            this.vuePendu.getChrono().resetTime();
            this.vuePendu.getChrono().start();
            this.vuePendu.modeJeu();
        }
        else {
            this.vuePendu.getChrono().stop();
            Optional<ButtonType> reponse = this.vuePendu.popUpPartieEnCours().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // si la réponse est oui
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                System.out.println("Ok !");
                this.vuePendu.init();
                this.vuePendu.modeJeu();
                this.modelePendu.setMotATrouver();
                this.vuePendu.getChrono().resetTime();
                this.vuePendu.getChrono().start();
                this.vuePendu.majAffichage();
            }
            else{
                System.out.println("D'ac !");
                this.vuePendu.getChrono().start(); // remet en route le chrono
            }
        }
    }
}
