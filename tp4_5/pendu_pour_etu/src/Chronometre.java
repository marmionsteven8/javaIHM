import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * Permet de gérer un Text associé à une Timeline pour afficher un temps écoulé
 */
public class Chronometre extends Text{
    /**
     * timeline qui va gérer le temps
     */
    private Timeline timeline;
    /**
     * la fenêtre de temps
     */
    private KeyFrame keyFrame;
    /**
     * le contrôleur associé au chronomètre
     */
    private ControleurChronometre actionTemps;
    /**
     * Vue du pendu
     */
    private Pendu vuePendu;
    /**
     * temps en seconde
     */
    private long seconde;
    /**
     * temps en minute
     */
    private long minute;
    /**
     * temps en heure
     */
    private long heure;

    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à "0:0:0"
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    public Chronometre(Pendu vuePendu){
        super("0:0:0");
        this.vuePendu = vuePendu;
        this.timeline = new Timeline();
        this.timeline.setCycleCount(Animation.INDEFINITE);
        this.actionTemps = new ControleurChronometre(this, this.vuePendu);
        this.keyFrame = new KeyFrame(Duration.seconds(1), actionTemps);
        this.timeline.getKeyFrames().add(keyFrame);
        //super.setText(this.getText());
        this.setText(String.valueOf(this.heure) + ":" + String.valueOf(this.minute) + ":" + String.valueOf(this.seconde));          // affiche 0:0:0
        super.setFont(new Font("arial", 10));
        super.setTextAlignment(TextAlignment.CENTER);
    }

    /**
     * Permet au controleur de mettre à jour le text
     * la durée est affichée sous la forme m:s
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec){
        //
    }

    /**
     * Permet de démarrer le chronomètre
     */
    public void start(){
        this.timeline.play();
    }

    /**
     * Permet d'arrêter le chronomètre
     */
    public void stop(){
        this.timeline.stop();
    }

    /**
     * Permet de remettre le chronomètre à 0
     */
    public void resetTime(){
        this.actionTemps.reset();
    }

    /**
     * @return le temps du chrono
     */
    public String getTime() {
        return String.valueOf(this.getHeure() + ":" + this.getMinute() + ":" + this.actionTemps.getTemps());
    }

    // getteurs
    /**
     * @return le temps en heure du chrono
     */
    public long getHeure() {
        return this.heure;
    }
    /**
     * @return le temps en minute du chrono
     */
    public long getMinute() {
        return this.minute;
    }
    /**
     * @return le temps en seconde du chrono
     */
    public long getSeconde() {
        return this.seconde;
    }

    // setteurs
    /**
     * @return le temps en heure du chrono
     */
    public void setHeure(long seconde) {
        this.heure = seconde;
    }
    /**
     * @return le temps en minute du chrono
     */
    public void setMinute(long minute) {
        this.minute = minute;
    }
    /**
     * @return le temps en seconde du chrono
     */
    public void setSeconde(long heure) {
        this.seconde = heure;
    }
}
