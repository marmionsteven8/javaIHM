import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Circle ;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane{
    /**
     * il est conseillé de stocker les touches dans un ArrayList
     */
    private List<Button> clavier;

    /**
     * constructeur du clavier
     * @param touches une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     * @param tailleLigne nombre de touches par ligne
     */
    public Clavier(String touches, EventHandler<ActionEvent> actionTouches) {
        super();
        this.setHgap(5);
        this.setVgap(5);
        this.clavier = new ArrayList<Button>();
        for (int i=0; i<touches.length(); ++i) {
            Character touche = touches.charAt(i);
            String toucheActuelle = touche.toString();
            Button b = new Button(toucheActuelle);
            b.setOnAction(actionTouches);
            this.clavier.add(b);
            this.getChildren().add(b);
        }
        this.setPrefWidth(8);
        this.setOrientation(Orientation.VERTICAL);
        this.setAlignment(Pos.CENTER);
    }

    /**
     * permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees){
        for (String touche: touchesDesactivees) {
            for (Button b: this.clavier) {
                if (b.getText().equals(touche)) {
                    b.setDisable(true);
                }
            }
        }
    }
}
