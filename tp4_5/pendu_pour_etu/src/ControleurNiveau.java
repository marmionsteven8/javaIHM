import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;

/**
 * Controleur des radio boutons gérant le niveau
 */
public class ControleurNiveau implements EventHandler<ActionEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;


    /**
     * @param modelePendu modèle du jeu
     */
    public ControleurNiveau(MotMystere modelePendu) {
        this.modelePendu = modelePendu;
    }

    /**
     * gère le changement de niveau
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        RadioButton radiobouton = (RadioButton) actionEvent.getTarget();
        String nomDuRadiobouton = radiobouton.getText();
        if (nomDuRadiobouton.equals("Facile")){
            int res = 0;
            this.modelePendu.setNiveau(res);
        }

        if (nomDuRadiobouton.equals("Medium")){
            int res = 1;
            this.modelePendu.setNiveau(res);
        }

        if (nomDuRadiobouton.equals("Difficile")){
            int res = 2;
            this.modelePendu.setNiveau(res);
        }

        if (nomDuRadiobouton.equals("Expert")){
            int res = 3;
            this.modelePendu.setNiveau(res);
        }
        System.out.println(nomDuRadiobouton);
    }
}
