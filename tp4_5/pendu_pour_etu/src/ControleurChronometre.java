import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Contrôleur du chronomètre
 */
public class ControleurChronometre implements EventHandler<ActionEvent> {
    /**
     * temps enregistré lors du dernier événement
     */
    private long tempsPrec;
    /**
     * temps écoulé depuis le début de la mesure
     */
    private long tempsEcoule;
    /**
     * Vue du chronomètre
     */
    private Chronometre chrono;
    /**
     * Vue du pendu 
     */
    private Pendu vuePendu; 

    /**
     * Constructeur du contrôleur du chronomètre
     * noter que le modèle du chronomètre est tellement simple
     * qu'il est inclus dans le contrôleur
     * @param chrono Vue du chronomètre
     */
    public ControleurChronometre (Chronometre chrono, Pendu vuePendu){
        this.chrono = chrono;
        this.tempsEcoule = 0;
        this.tempsPrec = 0;
        this.vuePendu = vuePendu;
    }

    /**
     * Actions à effectuer tous les pas de temps
     * essentiellement mesurer le temps écoulé depuis la dernière mesure
     * et mise à jour de la durée totale
     * @param actionEvent événement Action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        if (this.tempsEcoule+1>59) {
            if (this.chrono.getMinute()+1>59) {
                this.chrono.setHeure(this.chrono.getHeure()+1);
                this.tempsEcoule = 0;
                this.chrono.setTime(this.tempsEcoule);
                this.vuePendu.majAffichage();
            }
            else {
                this.chrono.setMinute(this.chrono.getMinute()+1);
                this.tempsEcoule = 0;
                this.chrono.setTime(this.tempsEcoule);
                this.vuePendu.majAffichage();
            }
        }
        else {
            this.tempsEcoule += 1;
            this.chrono.setTime(this.tempsEcoule);
            this.vuePendu.majAffichage();
        }
    }
    /**
     * Remet la durée à 0
     */
    public void reset(){
        this.tempsPrec = this.tempsEcoule;
        this.tempsEcoule = 0;
    }

    /**
     * @return le temps ecoulé actuel 
     */
    public long getTemps() {
        return this.tempsEcoule;
    }
}
