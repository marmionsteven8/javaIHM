import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TitledPane;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData ;
import javafx.scene.control.ButtonType ;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie)
     */ 
    private Button bJouer;
    /**
     * le temps du chrono affiché 
     */
    private Label tempsChrono;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("/usr/share/dict/french", 3, 10, MotMystere.FACILE, 10);
        //this.modelePendu = new MotMystere("american-english", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        // A terminer d'implementer
        this.niveaux = Arrays.asList("Facile", "Medium", "Difficile", "Expert");
        this.clavier = new Clavier("ABCEDFGHIJKLMNOPQRSTUVWXYZ-", new ControleurLettres(modelePendu, this));
        this.dessin = new ImageView(this.lesImages.get(0)); // ImageView
        this.pg = new ProgressBar(0); //ProgressBar
        this.bJouer = new Button("Lancer une partie");
        this.motCrypte = new Text(this.modelePendu.getMotCrypte()); // Label
        this.boutonMaison = new Button();
        this.boutonParametres = new Button();
        this.chrono = new Chronometre(this);
        this.chrono.resetTime();
        this.tempsChrono = new Label(String.valueOf(this.getChrono().getTime()));
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        this.init();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 800, 800);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private BorderPane titre(){
        // A implementer
        this.panelCentral = new BorderPane();        
        BorderPane banniere = new BorderPane();

        Label titreJeu = new Label("Jeu du pendu");
        titreJeu.setStyle("-fx-font-size: 2em;");

        HBox listeBouton = new HBox(5);

        Image imageH = new Image("file:img/home.png");
        ImageView imageHome = new ImageView(imageH);
        imageHome.setFitHeight(20);
        imageHome.setFitWidth(20);
        this.boutonMaison.setGraphic(imageHome);
        this.boutonMaison.setOnAction(new RetourAccueil(modelePendu, this));
        
        Image imageP = new Image("file:img/parametres.png");
        ImageView imageParametre = new ImageView(imageP);
        imageParametre.setFitHeight(20);
        imageParametre.setFitWidth(20);
        this.boutonParametres.setGraphic(imageParametre);
        this.boutonParametres.setOnAction(new ControleurInfos(this));

        Button infos = new Button();
        Image imageI = new Image("file:img/info.png");
        ImageView imageInfos = new ImageView(imageI);
        imageInfos.setFitHeight(20);
        imageInfos.setFitWidth(20);
        infos.setGraphic(imageInfos);
        infos.setOnAction(new ControleurInfos(this));

        listeBouton.getChildren().addAll(this.boutonMaison, this.boutonParametres, infos);

        banniere.setLeft(titreJeu);
        banniere.setRight(listeBouton);
        banniere.setPadding(new Insets(30, 10, 30, 20));

        this.panelCentral.setTop(banniere);
        banniere.setBackground((new Background(new BackgroundFill(Color.rgb(230,230,250), null, null))));

        return banniere;
    }

    // /**
     // * @return le panel du chronomètre
     // */
    // private TitledPane leChrono(){
        // A implementer
        // TitledPane res = new TitledPane();
        // return res;
    // }

    // /**
     // * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     // *         de progression et le clavier
     // */
    // private Pane fenetreJeu(){
        // A implementer
        // Pane res = new Pane();
        // return res;
    // }

    // /**
     // * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     // */
    // private Pane fenetreAccueil(){
        // A implementer    
        // Pane res = new Pane();
        // return res;
    // }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            //System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil(){
        VBox accueil = new VBox(10);

        accueil.getChildren().add(bJouer);
        this.bJouer.setOnAction(new ControleurLancerPartie(modelePendu, this));
        VBox.setMargin(bJouer, new Insets(20, 10, 20, 10));

        VBox toutLesNiveau = new VBox(10);
        ToggleGroup niveauxBoutons = new ToggleGroup();
        for (int i = 0; i<this.niveaux.size(); ++i) {
            RadioButton niveau = new RadioButton(this.niveaux.get(i));
            niveau.setToggleGroup(niveauxBoutons);
            niveau.setOnAction(new ControleurNiveau(modelePendu));
            toutLesNiveau.getChildren().add(niveau);
        }
        TitledPane content = new TitledPane("Niveau de difficulté", toutLesNiveau);
        accueil.getChildren().add(content);
        VBox.setMargin(content, new Insets(20, 10, 20, 10));
        this.panelCentral.setCenter(accueil);
        this.panelCentral.setRight(null);
    }
    
    public void modeJeu(){
        // Center du mode de Jeu
        VBox contentCenter = new VBox(10);

        contentCenter.setAlignment(Pos.CENTER);
        contentCenter.getChildren().addAll(this.motCrypte, this.dessin, this.pg, this.clavier);
        this.panelCentral.setCenter(contentCenter);
        

        // A droite du mode de Jeu
        VBox contentRight = new VBox(10);
        if (this.modelePendu.getNiveau()==0) {
            this.leNiveau = new Text("Niveau : Facile");
        }
        if (this.modelePendu.getNiveau()==1) {
            this.leNiveau = new Text("Niveau : Medium");
        }
        if (this.modelePendu.getNiveau()==2) {
            this.leNiveau = new Text("Niveau : Difficile");
        }
        if (this.modelePendu.getNiveau()==3) {
            this.leNiveau = new Text("Niveau : Expert");
        }
        contentRight.getChildren().add(this.leNiveau);
        BorderPane.setMargin(contentRight, new Insets(20));

        Button avoirUnNouveauMot = new Button("Nouveau Mot");
        avoirUnNouveauMot.setOnAction(new ControleurLancerPartie(modelePendu, this));

        TitledPane chronoTiltedPane = new TitledPane("Chronomètre", this.tempsChrono);
        chronoTiltedPane.setCollapsible(false); // permet d'éviter le dépliement du TiltedPane
        contentRight.getChildren().addAll(chronoTiltedPane , avoirUnNouveauMot);
        this.panelCentral.setRight(contentRight);
    }
    
    public void modeParametres(){
        this.popUpReglesDuJeu();
    }

    /** lance une partie */
    public void lancePartie(){
        this.init();
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
        this.dessin.setImage(this.lesImages.get(this.modelePendu.getNbErreursMax()-this.modelePendu.getNbErreursRestants()));
        double progress = (double)(this.modelePendu.getNbErreursMax()-this.modelePendu.getNbErreursRestants())/this.modelePendu.getNbErreursMax();
        this.pg.setProgress(progress);
        this.tempsChrono.setText(String.valueOf(this.chrono.getTime()));
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        return this.chrono;
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Règles du jeu");
        alert.setHeaderText("Lisez les règles");
        alert.setContentText("Voici les règles :\n-Cherchez le mot en tapant sur les touches données\n-Vous perdez si la bar de progression est au max\nBon jeu !");
        alert.showAndWait();
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION); 
        alert.setTitle("Message Gagnant"); 
        alert.setHeaderText("Vous avez gagné !"); 
        alert.setContentText("Le mot était : " + this.modelePendu.getMotATrouve());
        alert.showAndWait();
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Message Perdant");
        alert.setHeaderText("Vous avez perdu !"); 
        alert.setContentText("Le mot était : "+ this.modelePendu.getMotATrouve());
        alert.showAndWait();
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}

